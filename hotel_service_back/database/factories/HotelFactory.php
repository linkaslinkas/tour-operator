<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\City;
use App\Hotel;
use Faker\Generator as Faker;

$factory->define(Hotel::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'level' => $faker->randomFloat(1,0,5),
        'city_id' => factory(City::class)->create()->id,
    ];
});
