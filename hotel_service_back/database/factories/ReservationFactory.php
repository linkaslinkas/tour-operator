<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Apartment;
use App\Reservation;
use App\User;
use Faker\Generator as Faker;

$factory->define(Reservation::class, function (Faker $faker) {
    return [
        'apartment_id' => factory(Apartment::class)->create()->id,
        'user_id' => $faker->randomNumber(),
    ];
});
