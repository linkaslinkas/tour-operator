<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Apartment;
use App\Hotel;
use Faker\Generator as Faker;

$factory->define(Apartment::class, function (Faker $faker) {
    return [
        'number' => $faker->randomNumber(2),
        'rooms' => $faker->numberBetween(1,5),
        'cost' => $faker->numberBetween(0),
        'hotel_id' => factory(Hotel::class)->create()->id,
    ];
});
