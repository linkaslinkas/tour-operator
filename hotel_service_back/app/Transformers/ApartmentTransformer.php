<?php


namespace App\Transformers;


use App\Apartment;
use App\Table;
use Flugg\Responder\Transformers\Transformer;

class ApartmentTransformer extends Transformer
{
    protected $relations = [];

    protected $load = [
        'hotel' => HotelTransformer::class
    ];

    public function transform(Apartment $apartment)
    {
        return [
            'id' => $apartment->id,
            'number' => $apartment->number,
            'cost' => $apartment->cost,
            'rooms' => $apartment->rooms
        ];
    }

}
