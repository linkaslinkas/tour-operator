<?php


namespace App\Transformers;


use App\Country;
use Flugg\Responder\Transformers\Transformer;

class CountryTransformer extends Transformer
{
    protected $relations = [];

    protected $load = [];

    public function transform(Country $country)
    {
        return [
            'id' => $country->id,
            'name' => $country->name,
        ];
    }
}
