<?php


namespace App\Transformers;

use App\Hotel;
use Flugg\Responder\Transformers\Transformer;

class HotelTransformer extends Transformer
{
    protected $relations = [];

    protected $load = [
        'city' => CityTransformer::class
    ];

    public function transform(Hotel $hotel)
    {
        return [
            'id' => $hotel->id,
            'name' => $hotel->name,
            'level' => $hotel->level
        ];
    }

}
