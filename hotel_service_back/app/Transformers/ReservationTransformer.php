<?php


namespace App\Transformers;


use App\Reservation;
use Flugg\Responder\Transformers\Transformer;

class ReservationTransformer extends Transformer
{
    protected $relations = [];

    protected $load = [
        'apartment' => ApartmentTransformer::class
    ];

    public function transform(Reservation $reservation)
    {
        return [
            'id' => $reservation->id,
            'user_id' => $reservation->user_id,
            'from' => $reservation->from,
            'till' => $reservation->till,
        ];
    }

}
