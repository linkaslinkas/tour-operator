<?php

namespace App\Http\Middleware;

use App\Services\UserService;
use Closure;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');

        $userService = new UserService;
        $user = $userService->getAuthUser($token);

        if(!is_array($user)) {
            return responder()->error('unauthenticated')->respond(403);
        }

        return $next($request);
    }
}
