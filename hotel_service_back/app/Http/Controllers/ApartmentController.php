<?php


namespace App\Http\Controllers;

use App\Apartment;
use App\Table;
use App\Services\ApartmentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ApartmentController extends Controller
{
    const INDEX_RULES = [
        'hotel_id' => 'integer|exists:hotels,id',
        'rooms' => 'integer'
    ];
    public function index(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::INDEX_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $apartmentService = new ApartmentService;
        $apartments = $apartmentService->getApartments($params);

        return responder()->success($apartments)->respond(200);
    }

    const STORE_RULES = [
        'hotel_id' => 'required|exists:hotels,id',
        'number' => 'required|integer',
        'rooms' => 'required|integer',
        'cost' => 'required|integer'
    ];
    public function store(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::STORE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $apartmentService = new ApartmentService;
        $newApartment = $apartmentService->createApartment($params);

        return responder()->success($newApartment)->respond(201);
    }

    const UPDATE_RULES = [
        'id' => 'required|exists:apartments,id',
        'number' => 'integer',
        'rooms' => 'integer',
        'cost' => 'integer'
    ];
    public function update(Request $request, $id)
    {
        $params = $request->all();
        $params['id'] = $id;

        $validator = Validator::make($params, self::UPDATE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }
        $apartment = Apartment::find($id);

        $apartmentService = new ApartmentService($apartment);
        $newApartment = $apartmentService->updateApartment($params);

        return responder()->success($newApartment)->respond(200);
    }

    const DESTROY_RULES = [
        'id' => 'required|exists:apartments,id'
    ];
    public function destroy(Request $request, $id)
    {
        $params = $request->all();
        $params['id'] = $id;

        $validator = Validator::make($params, self::DESTROY_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }
        $apartment = Apartment::find($id);

        $apartmentService = new ApartmentService($apartment);
        $apartmentService->deleteApartment();

        return responder()->success(null)->respond(204);
    }
}
