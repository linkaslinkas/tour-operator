<?php


namespace App\Http\Controllers;


use App\Hotel;
use App\Restaurant;
use App\Services\HotelService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HotelController extends Controller
{
    const INDEX_RULES = [
        'city_id' => 'integer|exists:cities,id',
        'level' => 'float'
    ];
    public function index(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::INDEX_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $hotelService = new HotelService;
        $hotels = $hotelService->getHotels($params);

        return responder()->success($hotels)->respond(200);
    }

    const STORE_RULES = [
        'city_id' => 'integer|exists:cities,id',
        'name' => 'required|string',
        'level' => 'required'
    ];
    public function store(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::STORE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $hotelService = new HotelService;
        $newHotel = $hotelService->createHotel($params);

        return responder()->success($newHotel)->respond(201);
    }

    const UPDATE_RULES = [
        'id' => 'required|exists:hotels,id',
        'name' => 'string',
        'level' => 'float',
        'city_id' => 'exists:cities,id'
    ];
    public function update(Request $request, $id)
    {
        $params = $request->all();
        $params['id'] = $id;

        $validator = Validator::make($params, self::UPDATE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }
        $hotel = Hotel::find($id);

        $hotelService = new HotelService($hotel);
        $newHotel = $hotelService->updateHotel($params);

        return responder()->success($newHotel)->respond(200);
    }

    const DESTROY_RULES = [
        'id' => 'required|exists:hotels,id'
    ];
    public function destroy(Request $request, $id)
    {
        $params = $request->all();
        $params['id'] = $id;

        $validator = Validator::make($params, self::DESTROY_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }
        $hotel = Hotel::find($id);

        $hotelService = new HotelService($hotel);
        $hotelService->deleteHotel();

        return responder()->success(null)->respond(204);
    }
}
