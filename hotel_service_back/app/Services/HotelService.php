<?php


namespace App\Services;


use App\Hotel;
use Illuminate\Support\Facades\Schema;

class HotelService
{
    protected $hotel;

    public function __construct(Hotel $hotel = null)
    {
        $this->hotel = $hotel;
    }

    public function getHotels(array $filters)
    {
        $hotels = Hotel::all();

        $hotels = $hotels->filter(function ($hotel) use ($filters) {
            foreach ($filters as $field => $value) {
                if (Schema::hasColumn($hotel->getTable(), $field) && ($hotel->$field) != $value) {
                    return false;
                }
            }
            return true;
        });

        return $hotels->sortByDesc('level');
    }

    public function createHotel($data)
    {
        $newHotel = Hotel::create($data);

        return $newHotel;
    }

    public function updateHotel($data)
    {
        $this->hotel->update($data);

        $hotel = $this->hotel->fresh();

        return $hotel;
    }

    public function deleteHotel()
    {
        foreach ($this->hotel->apartments as $apartment)
        {
            (new ApartmentService($apartment))->deleteApartment();
        }

        $this->hotel->delete();
    }
}
