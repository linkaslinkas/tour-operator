<?php


namespace App\Services;


use App\Apartment;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;

class ApartmentService
{
    protected $apartment;

    public function __construct(Apartment $apartment = null)
    {
        $this->apartment = $apartment;
    }

    public function getApartments(array $filters)
    {
        $apartments = Apartment::all();

        $apartments = $apartments->filter(function ($apartment) use ($filters) {
            foreach ($filters as $field => $value) {
                if (Schema::hasColumn($apartment->getTable(), $field) && ($apartment->$field) != $value) {
                    return false;
                }
            }
            return true;
        });

        return $apartments->sortBy('rooms');
    }

    public function createApartment($data)
    {
        $newApartment = Apartment::create($data);

        return $newApartment;
    }

    public function updateApartment($data)
    {
        $this->apartment->update($data);

        $apartment = $this->apartment->fresh();

        return $apartment;
    }

    public function getFreeApartment($data)
    {
        $apartments = Apartment::whereHas('hotel', function ($query) use ($data) {
            return $query->whereHas('city', function ($q) use ($data) {
                return $q->where(['cities.name' => $data['city']]);
            });
        })->orderBy('cost')->get();

        $apartment = $this->checkIfFree($apartments, $data['dates']);

        return $apartment;
    }

    private function checkIfFree($apartments, $dates)
    {
        foreach ($apartments as $apartment) {
            foreach ($apartment->reservations as $reservation) {
                if($reservation->from > Carbon::createFromFormat('Y-m-d', $dates[0])
                    || $reservation->till < Carbon::createFromFormat('Y-m-d', $dates[1])) {
                    return false;
                }
            }
            return $apartment;
        }
    }

    public function deleteApartment()
    {
        foreach ($this->apartment->reservations as $reservation)
        {
            (new ReservationService($reservation))->deleteReservation();
        }

        $this->apartment->delete();
    }
}
