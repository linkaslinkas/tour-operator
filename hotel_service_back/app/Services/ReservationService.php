<?php

namespace App\Services;

use App\Reservation;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Schema;

class ReservationService
{
    protected $reservation;

    public function __construct(Reservation $reservation = null)
    {
        $this->reservation = $reservation;
    }

    public function getReservations($filters)
    {
        $reservations = Reservation::all();

        $reservations = $reservations->filter(function ($reservation) use ($filters) {
            foreach ($filters as $field => $value) {
                if (Schema::hasColumn($reservation->getTable(), $field) && ($reservation->$field) != $value) {
                    return false;
                }
            }
            return true;
        });

        return $reservations;
    }

    public function createReservation($data)
    {
        $freeApartment = (new ApartmentService)->getFreeApartment($data);
        if(!$freeApartment) {
            return null;
        }

        $data['apartment_id'] = $freeApartment->id;
        $data['from'] = $data['dates'][0];
        $data['till'] = $data['dates'][1];

        $newReservation = Reservation::create($data);

        $client = new Client();
        $params = [
            'form_params' => array_merge($data,
                ['description' => 'Apartment n ' . $freeApartment->number . ' in ' . $freeApartment->hotel->name,
                    'charge' => $freeApartment->cost,
                    'spec_id' => 'h'.$freeApartment->id,])
        ];

        $paymentResponse = $client->post(config('app.payment_service') . '/charges', $params);

        return $newReservation;
    }

    public function updateReservation($data)
    {
        $this->reservation->update($data);

        $newReservation = $this->reservation->fresh();

        $client = new Client();
        $params = [
            'form_params' => [
                'spec_id' => 'h'.$this->reservation->apartment_id,
                'charge' => $this->reservation->apartment->cost,
                'description' => 'Table n ' . $this->reservation->apartment->number . ' in ' . $this->reservation->apartment->hotel->name,
            ]
        ];

        $client->put(config('app.payment_service') . '/charges/update?XDEBUG_SESSION_START=PHPSTORM', $params);


        return $newReservation;
    }

    public function deleteReservation()
    {
        $this->reservation->delete();
    }
}
