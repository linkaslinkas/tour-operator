<?php


namespace App;

use App\Transformers\ReservationTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model implements Transformable
{
    public $table = 'reservations';

    protected $fillable = [
        'user_id',
        'apartment_id',
        'from',
        'till'
    ];

    public function transformer()
    {
        return ReservationTransformer::class;
    }

    public function apartment()
    {
        return $this->belongsTo(Apartment::class);
    }
}
