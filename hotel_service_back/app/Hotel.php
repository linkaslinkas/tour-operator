<?php


namespace App;


use App\Transformers\HotelTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Hotel extends Model implements Transformable
{
    public $table = 'hotels';

    protected $fillable = [
        'city_id',
        'level',
        'name'
    ];

    public function transformer()
    {
        return HotelTransformer::class;
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function apartments()
    {
        return $this->hasMany(Apartment::class);
    }
}
