<?php


namespace App;


use App\Transformers\ApartmentTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Apartment extends Model implements Transformable
{
    public $table = 'apartments';

    protected $fillable = [
        'hotel_id',
        'number',
        'rooms',
        'cost'
    ];

    public function transformer()
    {
        return ApartmentTransformer::class;
    }

    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }
}
