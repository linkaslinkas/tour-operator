<?php


namespace Tests\Feature\Reservation;


use App\Reservation;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class getReservationsTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_gets_all_reservations()
    {
        factory(Reservation::class, 5)->create();

        // ACT
        $response = $this->get('soa/service2/reservations');

        // ASSERT
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson()['data'];

        $this->assertCount(5, $responseData);
    }
}
