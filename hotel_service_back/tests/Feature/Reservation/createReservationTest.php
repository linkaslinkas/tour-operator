<?php


namespace Tests\Feature\Reservation;


use App\Table;
use App\City;
use App\Country;
use App\Restaurant;
use App\Services\ApartmentService;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class createReservationTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_creates_a_new_reservation()
    {
        $targetUser = factory(User::class)->create();
        $apartment = factory(Table::class)->create();
        $apartment1 = factory(Table::class)->create();

        $data = [
            'user_id' => $targetUser->id,
            'city' => $apartment1->hotel->city->name,
            'dates' => ['2020-01-01', '2020-05-05']
        ];

        // ACT
        $response = $this->post('soa/service2/reservations', $data);

        // ASSERT
        $response->assertStatus(201);
        $this->assertDatabaseHas('reservations', [
            'user_id' => $targetUser->id,
            'apartment_id' => $apartment1->id
        ]);
    }
}
