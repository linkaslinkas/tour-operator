<?php


namespace Tests\Feature\City;


use App\City;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class getCitiesTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_gets_all_countries()
    {
        factory(City::class, 5)->create();

        // ACT
        $response = $this->get('soa/service2/cities');

        // ASSERT
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson()['data'];

        $this->assertCount(5, $responseData);
    }
}
