<?php


namespace Tests\Feature\City;


use App\Country;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class createCityTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_creates_a_new_city()
    {
        $country = factory(Country::class)->create();
        $data = [
            'country_id' => $country->id,
            'name' => 'City'
        ];

        // ACT
        $response = $this->post('soa/service2/cities', $data);

        // ASSERT
        $response->assertStatus(201);
        $this->assertDatabaseHas('cities', $data);
    }
}
