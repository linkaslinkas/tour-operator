<?php


use App\City;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class createHotelTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_creates_a_new_hotel()
    {
        $city = factory(City::class)->create();
        $data = [
            'city_id' => $city->id,
            'level' => 4.5,
            'name' => 'Hotel'
        ];

        // ACT
        $response = $this->post('soa/service2/hotels', $data);

        // ASSERT
        $response->assertStatus(201);
        $this->assertDatabaseHas('hotels', $data);
    }
}
