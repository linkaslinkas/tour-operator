<?php


use App\Hotel;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class getHotelsTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_gets_all_hotels()
    {
        factory(Hotel::class, 5)->create();

        // ACT
        $response = $this->get('soa/service2/hotels');

        // ASSERT
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson()['data'];

        $this->assertCount(5, $responseData);
    }
}
