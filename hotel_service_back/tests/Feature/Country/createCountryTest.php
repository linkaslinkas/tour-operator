<?php


namespace Tests\Feature\Country;


use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class createCountryTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_creates_new_country()
    {
        $data = [
            'name' => 'Country'
        ];

        // ACT
        $response = $this->post('soa/service2/countries', $data);

        // ASSERT
        $response->assertStatus(201);
        $this->assertDatabaseHas('countries', $data);
    }
}
