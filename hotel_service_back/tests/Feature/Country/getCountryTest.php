<?php


use App\Country;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class getCountryTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_gets_all_countries()
    {
        factory(Country::class, 5)->create();

        // ACT
        $response = $this->get('soa/service2/countries');

        // ASSERT
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson()['data'];

        $this->assertCount(5, $responseData);
    }

    /** @test */
    public function it_gets_country_filtered_by_name()
    {
        $country = factory(Country::class)->create();
        factory(Country::class, 4)->create();

        // ACT
        $response = $this->get('soa/service2/countries?name=' . $country->name);

        // ASSERT
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson()['data'];

        $this->assertEquals(5, Country::all()->count());
        $this->assertCount(1, $responseData);
        $this->assertEquals($country->id, $responseData[0]['id']);
    }
}
