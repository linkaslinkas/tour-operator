import { Component, DoCheck } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements DoCheck {
  title = 'tour-ui';
  userName: string;
  isLogged: boolean;
  opened: boolean;
  role: string;

  constructor(private router: Router) {
    this.role = localStorage.getItem('role');
    const name = localStorage.getItem('userName');
    name ? this.userName = name : this.router.navigateByUrl('login');
  }

  ngDoCheck(): void {
    const name = localStorage.getItem('userName');
    name ? this.isLogged = true : this.isLogged = false;
  }

  onSignOut(): void {
    this.isLogged = false;
    localStorage.clear();
    this.router.navigateByUrl('login');
  }
}
