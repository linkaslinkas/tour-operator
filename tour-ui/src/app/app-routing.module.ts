import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TourComponent } from './components/tour/tour.component';
import { TourListComponent } from './components/tour-list/tour-list.component';
import { LoginComponent } from './components/login/login.component';
import { CountriesComponent } from './components/countries/countries.component';
import { CitiesComponent } from './components/cities/cities.component';
import { ToursComponent } from './components/tours/tours.component';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'tour',
    component: TourComponent,
  },
  {
    path: 'reservations',
    component: TourListComponent
  },
  {
    path: 'countries',
    component: CountriesComponent
  },
  {
    path: 'cities',
    component: CitiesComponent
  },
  {
    path: 'tours',
    component: ToursComponent
  },
  {
    path: '**',
    redirectTo: '/reservations',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
