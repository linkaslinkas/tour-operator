import { ICountryData } from './country';

export interface ICity {
  data: ICityData[];
}

export interface ICityData {
    id: number;
    name: string;
    visits: string;
    country: ICountryData;
}
