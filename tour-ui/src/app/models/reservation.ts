import { ITour } from './tour';
import { IUser } from './user';

export interface IReservation {
  data: IReservationData[];
}

export interface IReservationData {
    id: number;
    tour: ITour;
    user: IUser;
}
