import { ICityData } from './city';

export interface ITour {
  data: ITourData[];
}

export interface ITourData {
  id: number;
  name: string;
  description: string;
  cost: number;
  city: ICityData;
}
