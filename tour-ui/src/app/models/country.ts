export interface ICountry {
  data: ICountryData[];
}

export interface ICountryData {
    id: number;
    name: string;
    visits: string;
}
