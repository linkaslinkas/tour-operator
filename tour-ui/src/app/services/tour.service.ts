import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IReservation } from '../models/reservation';
import { ICountry } from '../models/country';
import { ICity } from '../models/city';
import { ITour } from '../models/tour';

@Injectable({
  providedIn: 'root'
})
export class TourService {

  httpOptions = {
    headers: new HttpHeaders({
      'Authorization': localStorage.getItem('token')
    })
  };

  constructor(private http: HttpClient) { }

  getReservations(): Observable<IReservation> {
    return this.http.get<IReservation>('http://localhost:8000/soa/service1/reservations', this.httpOptions);
  }

  deleteReservation(id: number): Observable<void> {
    return this.http.delete<void>('http://localhost:8000/soa/service1/reservations/' + id, this.httpOptions);
  }

  getCountries(): Observable<ICountry> {
    return this.http.get<ICountry>('http://localhost:8000/soa/service1/countries', this.httpOptions);
  }

  addCountry(name: string): Observable<void> {
    return this.http.post<void>('http://localhost:8000/soa/service1/countries', {name}, this.httpOptions);
  }

  getCities(countryId: number): Observable<ICity> {
    return this.http.get<ICity>('http://localhost:8000/soa/service1/cities?country_id=' + countryId, this.httpOptions);
  }

  getAllCities(): Observable<ICity> {
    return this.http.get<ICity>('http://localhost:8000/soa/service1/cities', this.httpOptions);
  }

  addCity(countryId: number, name: string): Observable<void> {
    return this.http.post<void>('http://localhost:8000/soa/service1/cities', {country_id: countryId, name}, this.httpOptions);
  }

  getTours(cityId: number): Observable<ITour> {
    return this.http.get<ITour>('http://localhost:8000/soa/service1/tours?city_id=' + cityId, this.httpOptions);
  }

  addReservation(data: any): Observable<void> {
    return this.http.post<void>('http://localhost:8000/soa/service1/reservations', data, this.httpOptions);
  }
}
