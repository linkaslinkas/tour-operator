import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IUser } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  signIn(user: IUser): Observable<any> {
    return this.http.post<any>('http://localhost:8000/soa/service1/login', user);
  }
}
