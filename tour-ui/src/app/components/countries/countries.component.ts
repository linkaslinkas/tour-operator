import { Component, OnInit } from '@angular/core';
import { TourService } from 'src/app/services/tour.service';
import { ICountryData } from 'src/app/models/country';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { AddCountriesDialogComponent } from './add-countries-dialog/add-countries-dialog.component';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.sass']
})
export class CountriesComponent implements OnInit {

  countries: ICountryData[];
  displayedColumns: string[] = ['name', 'visits', 'action'];
  dataSource: MatTableDataSource<any>;

  constructor(private tourService: TourService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getCountries();
  }

  getCountries(): void {
    this.tourService.getCountries().subscribe(res => {
      this.countries = res.data;
      this.dataSource = new MatTableDataSource(this.countries);
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddCountriesDialogComponent, {
      width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.tourService.addCountry(result).subscribe(() => {
          this.getCountries();
        });
      }
    });
  }

}
