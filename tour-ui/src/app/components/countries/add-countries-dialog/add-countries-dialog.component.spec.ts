import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCountriesDialogComponent } from './add-countries-dialog.component';

describe('AddCountriesDialogComponent', () => {
  let component: AddCountriesDialogComponent;
  let fixture: ComponentFixture<AddCountriesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCountriesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCountriesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
