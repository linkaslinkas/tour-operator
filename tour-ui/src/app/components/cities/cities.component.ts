import { Component, OnInit } from '@angular/core';
import { ICityData } from 'src/app/models/city';
import { MatTableDataSource } from '@angular/material/table';
import { TourService } from 'src/app/services/tour.service';
import { MatDialog } from '@angular/material/dialog';
import { AddCitiesDialogComponent } from './add-cities-dialog/add-cities-dialog.component';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.sass']
})
export class CitiesComponent implements OnInit {


  cities: ICityData[];
  displayedColumns: string[] = ['name', 'visits', 'country', 'action'];
  dataSource: MatTableDataSource<any>;

  constructor(private tourService: TourService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getCities();
  }

  getCities(): void {
    this.tourService.getAllCities().subscribe(res => {
      this.cities = res.data;
      this.dataSource = new MatTableDataSource(this.cities);
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddCitiesDialogComponent, {
      width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.tourService.addCity(result.countryId, result.name).subscribe(() => {
          this.getCities();
        });
      }
    });
  }
}
