import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCitiesDialogComponent } from './add-cities-dialog.component';

describe('AddCitiesDialogComponent', () => {
  let component: AddCitiesDialogComponent;
  let fixture: ComponentFixture<AddCitiesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCitiesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCitiesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
