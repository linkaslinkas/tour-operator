import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ICountryData } from 'src/app/models/country';
import { TourService } from 'src/app/services/tour.service';

@Component({
  selector: 'app-add-cities-dialog',
  templateUrl: './add-cities-dialog.component.html',
  styleUrls: ['./add-cities-dialog.component.sass']
})
export class AddCitiesDialogComponent {

  name: string;
  countries: ICountryData[];
  countryId: number;

  constructor(
    public dialogRef: MatDialogRef<AddCitiesDialogComponent>,
    private tourService: TourService
  ) {
      this.tourService.getCountries().subscribe(res => {
        this.countries = res.data;
      });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    this.dialogRef.close({countryId: this.countryId, name: this.name});
  }

}
