import { Component, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { TourService } from 'src/app/services/tour.service';
import { IReservationData } from 'src/app/models/reservation';

@Component({
  selector: 'app-tour-list',
  templateUrl: './tour-list.component.html',
  styleUrls: ['./tour-list.component.sass']
})
export class TourListComponent {

  displayedColumns: string[] = ['name', 'description', 'cost', 'action'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  reservations: IReservationData[];

  constructor(private tourService: TourService) {
    this.getReservations();
  }

  getReservations() {
    this.tourService.getReservations().subscribe(res => {
      this.reservations = res.data;
      this.dataSource = new MatTableDataSource(this.reservations);
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onDelete(id: number): void {
    this.tourService.deleteReservation(id).subscribe(() => {
      this.getReservations();
    });
  }

}
