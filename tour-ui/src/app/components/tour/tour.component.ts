import { Component, OnInit } from '@angular/core';
import { TourService } from 'src/app/services/tour.service';
import { ICountryData } from 'src/app/models/country';
import { ICityData } from 'src/app/models/city';
import { ITourData } from 'src/app/models/tour';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-tour',
  templateUrl: './tour.component.html',
  styleUrls: ['./tour.component.sass']
})
export class TourComponent implements OnInit {

  step: number;
  countries: ICountryData[];
  countryId: number;
  cities: ICityData[];
  cityId: number;
  tours: ITourData[];
  tourId: number;
  displayedColumns: string[] = ['name', 'description', 'cost', 'action'];
  dataSource: MatTableDataSource<any>;
  startDate = new FormControl(new Date());
  endDate = new FormControl(new Date());

  constructor(
    private tourService: TourService,
    private router: Router,
    private datePipe: DatePipe
  ) {}

  ngOnInit(): void {
    this.tourService.getCountries().subscribe(res => {
      this.countries = res.data;
    });
  }

  onFirstStep(): void {
    this.step = 1;
  }

  onSecondStep(): void {
    this.tourService.getCities(this.countryId).subscribe(res => {
      this.cities = res.data;
    });
    this.step = 2;
  }

  onThirdStep(): void {
    this.tourService.getTours(this.cityId).subscribe(res => {
      this.tours = res.data;
      this.dataSource = new MatTableDataSource(this.tours);
    });
    this.step = 3;
  }

  onSelectRow(id: number): void {
    this.tourId = id;
  }

  onSubmit(): void {
    const firstDate = this.datePipe.transform(this.startDate.value, 'yyyy-MM-dd');
    const secondDate = this.datePipe.transform(this.endDate.value, 'yyyy-MM-dd');
    const data = {
      tour_id: this.tourId,
      dates: firstDate + '_' + secondDate
    };
    this.tourService.addReservation(data).subscribe(() => {
      this.router.navigateByUrl('tour-list');
    });
  }

}
