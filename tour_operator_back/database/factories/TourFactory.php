<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\City;
use App\Tour;
use Faker\Generator as Faker;

$factory->define(Tour::class, function (Faker $faker) {
    return [
        "name" => $faker->country,
        'description' => $faker->text,
        'cost' => $faker->numberBetween(0),
        'city_id' => factory(City::class)->create()->id,
    ];
});
