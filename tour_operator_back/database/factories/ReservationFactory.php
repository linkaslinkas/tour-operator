<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Reservation;
use App\User;
use App\Tour;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Reservation::class, function (Faker $faker) {
    return [
        'tour_id' => factory(Tour::class)->create()->id,
        'user_id' => 1,
        'from' => Carbon::now(),
        'till' => Carbon::tomorrow(),
    ];
});
