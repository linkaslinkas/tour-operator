<?php


namespace Tests\Feature\Country;


use App\Country;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class createCountryTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_creates_new_country()
    {
        $data = [
            'name' => '2D Land'
        ];

        // ACT
        $response = $this->post('soa/service1/countries', $data);
        $responseData = $response->decodeResponseJson()['data'];

        // ASSERT
        $response->assertStatus(201);
        $this->assertEquals(1, Country::all()->count());
    }
}
