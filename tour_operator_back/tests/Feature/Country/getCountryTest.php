<?php


namespace Tests\Feature\Country;


use App\Country;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class getCountryTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_gets_all_organisations()
    {
        factory(Country::class, 5)->create();

        // ACT
        $response = $this->get('soa/service1/countries');
        $responseData = $response->decodeResponseJson()['data'];

        // ASSERT
        $response->assertStatus(200);
        $this->assertCount(5, $responseData);
    }

    /** @test */
    public function it_gets_countries_filtered_by_name()
    {
        $countries = factory(Country::class, 5)->create();

        // ACT
        $response = $this->get('soa/service1/countries?name=' . $countries->first()->name);
        $responseData = $response->decodeResponseJson()['data'];

        // ASSERT
        $response->assertStatus(200);
        $this->assertCount(1, $responseData);
    }
}
