<?php


namespace Tests\Feature\City;

use App\City;
use App\Country;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class getCitiesTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_gets_all_cities()
    {
        factory(City::class, 5)->create();

        // ACT
        $response = $this->get('soa/service1/cities');

        // ASSERT
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson()['data'];

        $this->assertCount(5, $responseData);
    }

    /** @test */
    public function it_gets_all_cities_filtered_by_country()
    {
        $country = factory(Country::class)->create();
        factory(City::class, 5)->create(['country_id' => $country->id]);
        factory(City::class, 10)->create();

        // ACT
        $response = $this->get('soa/service1/cities?country_id=' . $country->id);

        // ASSERT
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson()['data'];

        $this->assertCount(5, $responseData);
    }
}
