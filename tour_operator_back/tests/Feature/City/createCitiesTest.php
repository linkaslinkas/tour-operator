<?php


namespace Tests\Feature\City;


use App\Country;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class createCitiesTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_creates_a_new_city()
    {
        $country = factory(Country::class)->create();
        $data = [
            'country_id' => $country->id,
            'name' => 'City'
        ];

        // ACT
        $response = $this->post('soa/service1/cities', $data);

        // ASSERT
        $response->assertStatus(201);
        $this->assertDatabaseHas('cities', $data);
    }
}
