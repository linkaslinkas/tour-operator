<?php


namespace Tests\Feature\Reservation;


use App\Reservation;
use App\Tour;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class createReservationTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_creates_new_reservation()
    {
        $user = factory(User::class)->create();
        $tour = factory(Tour::class)->create();

        $data = [
            'user_id' => $user->id,
            'tour_id' => $tour->id,
            'dates' => '2020-01-01_2020-05-05'
        ];

        // ACT
        $response = $this->post('soa/service1/reservations', $data);

        // ASSERT
        $response->assertStatus(201);
        $this->assertEquals(1, Reservation::all()->count());
    }
}
