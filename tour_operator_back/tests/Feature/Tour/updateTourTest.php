<?php


namespace Tests\Feature\Tour;


use App\Tour;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class updateTourTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_updates_tour()
    {
        $tour = factory(Tour::class)->create();

        $data = [
            'name' => 'Updated',
            'cost' => 250
        ];

        // ACT
        $response = $this->put('soa/service1/tours/' . $tour->id, $data);

        // ASSERT
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson()['data'];

        $this->assertDatabaseHas('tours', $data);
    }
}
