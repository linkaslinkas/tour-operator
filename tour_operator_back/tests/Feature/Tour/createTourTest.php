<?php


namespace Tests\Feature\Tour;


use App\City;
use App\Tour;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class createTourTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_creates_tour()
    {
        $data = [
            'name' => 'Tour',
            'description' => 'Tour',
            'cost' => 100,
            'city_id' => factory(City::class)->create()->id,
        ];

        // ACT
        $response = $this->post('soa/service1/tours', $data);

        // ASSERT
        $response->assertStatus(201);
        $this->assertEquals(1, Tour::all()->count());
        $this->assertDatabaseHas('tours', $data);
    }
}
