<?php


namespace Tests\Feature\Tour;


use App\City;
use App\Tour;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class getToursTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_gets_all_tours()
    {
        factory(Tour::class, 5)->create();

        // ACT
        $response = $this->get('soa/service1/tours');
        $responseData = $response->decodeResponseJson()['data'];

        // ASSERT
        $response->assertStatus(200);
        $this->assertCount(5, $responseData);
    }

    /** @test */
    public function it_gets_tours_filtered_by_city_id()
    {
        $city = factory(City::class)->create();
        factory(Tour::class, 5)->create(['city_id' => $city->id]);
        factory(Tour::class, 10)->create();

        // ACT
        $response = $this->get('soa/service1/tours?city_id=' . $city->id);
        $responseData = $response->decodeResponseJson()['data'];

        // ASSERT
        $response->assertStatus(200);
        $this->assertCount(5, $responseData);
    }
}
