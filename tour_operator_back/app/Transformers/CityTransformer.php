<?php


namespace App\Transformers;

use App\City;
use Flugg\Responder\Transformers\Transformer;

class CityTransformer extends Transformer
{
    protected $relations = [];

    protected $load = [
        'country' => CountryTransformer::class,
    ];

    public function transform(City $city)
    {
        return [
            'id' => $city->id,
            'name' => $city->name,
            'visits' => $city->visits
        ];
    }
}
