<?php


namespace App\Transformers;


use App\Reservation;
use Flugg\Responder\Transformers\Transformer;

class ReservationTransformer extends Transformer
{
    protected $relations = [];

    protected $load = [
        'tour' => TourTransformer::class,
    ];

    public function transform(Reservation $reservation)
    {
        return [
            'id' => $reservation->id,
        ];
    }
}
