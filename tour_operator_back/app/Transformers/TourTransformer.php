<?php


namespace App\Transformers;

use App\Tour;
use Flugg\Responder\Transformers\Transformer;

class TourTransformer extends Transformer
{
    protected $relations = [];

    protected $load = [
        'city' => CityTransformer::class
    ];

    public function transform(Tour $tour)
    {
        return [
            'id' => $tour->id,
            'name' => $tour->name,
            'description' => $tour->description,
            'cost' => $tour->cost,
        ];
    }

}
