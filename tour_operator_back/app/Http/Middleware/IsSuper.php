<?php

namespace App\Http\Middleware;

use App\Enums\RoleEnum;
use App\Services\UserService;
use Closure;

class IsSuper
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');

        $userService = new UserService;
        $user = $userService->getAuthUser($token);

        if($user['role'] < RoleEnum::ADMIN) {
            return responder()->error('permission_error')->respond(403);
        }

        return $next($request);
    }
}
