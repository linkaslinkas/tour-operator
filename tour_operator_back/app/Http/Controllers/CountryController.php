<?php


namespace App\Http\Controllers;

use App\Services\CountryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CountryController extends Controller
{
    const INDEX_RULES = [
        'name' => 'string'
    ];
    public function index(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::INDEX_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $countryService = new CountryService();
        $countries = $countryService->getCountries($params);

        return responder()->success($countries)->respond(200);
    }

    const STORE_RULES = [
        'name' => 'required|string',
    ];
    public function store(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::STORE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $countryService = new CountryService;
        $newCountry = $countryService->createCountry($params);

        return responder()->success($newCountry)->respond(201);
    }
}
