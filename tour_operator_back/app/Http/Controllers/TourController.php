<?php


namespace App\Http\Controllers;

use App\Services\TourService;
use App\Tour;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TourController extends Controller
{

    const INDEX_RULES = [
        'city_id' => 'nullable|integer|exists:cities,id'
    ];
    public function index(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::INDEX_RULES);
        if ($validator->fails()) {
            return responder()->error('validation error')->respond(403);
        }

        $tourService = new TourService;
        $tours = $tourService->getTours($params);

        return responder()->success($tours)->respond(200);
    }

    const STORE_RULES = [
        'name' => 'required|string',
        'description' => 'nullable|string',
        'cost' => 'required|integer',
        'city_id' => 'required|integer|exists:cities,id'
    ];
    public function store(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::STORE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $tourService = new TourService;
        $newTour = $tourService->createTour($params);

        return responder()->success($newTour)->respond(201);
    }

    const SHOW_RULES = [
        'tour_id' => 'required|integer|exists:tours,id'
    ];
    public function show(Request $request, $tour_id)
    {
        $params = $request->all();
        $params['tour_id'] = $tour_id;

        $validator = Validator::make($params, self::SHOW_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $tourService = new TourService;
        $tour = $tourService->getTour($tour_id);

        return responder()->success($tour)->respond(200);
    }

    const UPDATE_RULES = [
        'name' => 'nullable|string',
        'description' => 'nullable|string',
        'cost' => 'nullable|integer',
        'tour_id' => 'required|integer|exists:tours,id'
    ];
    public function update(Request $request, $tour_id)
    {
        $params = $request->all();
        $params['tour_id'] = $tour_id;

        $validator = Validator::make($params, self::UPDATE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $tour = Tour::find($tour_id);

        $tourService = new TourService($tour);
        $updatedTour = $tourService->updateTour($params);

        return responder()->success($updatedTour)->respond(200);
    }

    const DESTROY_RULES = [
        'id' => 'required|exists:tours,id',
    ];
    public function destroy(Request $request, $id)
    {
        $params = $request->all();
        $params['id'] = $id;

        $validator = Validator::make($params, self::DESTROY_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $tour = Tour::find($id);

        $tourService = new TourService($tour);
        $tourService->deleteTour();

        return responder()->success()->respond(204);
    }
}
