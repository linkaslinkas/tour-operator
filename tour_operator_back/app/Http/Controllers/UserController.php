<?php


namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    const STORE_RULES = [
        'name' => 'required|string',
        'email' => 'required|string',
        'password' => 'required'
    ];

    public function store(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::STORE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $userService = new UserService;
        $newUser = $userService->createUser($params);

        if(is_array($newUser)) {
            return responder()->success($newUser)->respond(201);
        } else {
            return responder()->error('not_valid_data')->respond(400);
        }
    }

    public function login(Request $request)
    {
        $params = $request->all();

        $userService = new UserService;
        $token = $userService->login($params);

        if(is_array($token)) {
            return responder()->success($token)->respond(201);
        } else {
            return responder()->error('wrong_credentials')->respond(400);
        }
    }
}
