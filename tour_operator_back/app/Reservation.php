<?php


namespace App;


use App\Transformers\ReservationTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model implements Transformable
{
    public $table = 'reservations';

    protected $fillable = [
        'tour_id',
        'user_id',
        'from',
        'till'
    ];

    public function transformer()
    {
        return ReservationTransformer::class;
    }

    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }
}
