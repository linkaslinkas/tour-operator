<?php


namespace App\Services;


use App\Country;
use Illuminate\Support\Facades\Schema;

class CountryService
{
    public function getCountries($filters)
    {
        $countries = Country::all();

        $countries = $countries->filter(function ($country) use ($filters){
            foreach ($filters as $field=>$value){
                if (Schema::hasColumn($country->getTable(), $field) && ($country->$field) != $value) {
                    return false;
                }
            }
            return true;
        });

        return $countries->sortByDesc('visits');
    }

    public function createCountry($data)
    {
        $newCountry = Country::create($data);

        return $newCountry;
    }
}
