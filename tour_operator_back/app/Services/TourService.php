<?php


namespace App\Services;


use App\Tour;
use Illuminate\Support\Facades\Schema;

class TourService
{
    private $tour;

    public function __construct(Tour $tour = null)
    {
        $this->tour = $tour;
    }

    public function getTours($filters)
    {
        $tours = Tour::all();

        $tours = $tours->filter(function ($tour) use ($filters) {
            foreach ($filters as $field => $value) {
                if (Schema::hasColumn($tour->getTable(), $field) && ($tour->$field) != $value) {
                    return false;
                }
            }
            return true;
        });

        return $tours;
    }

    public function getTour($id): Tour
    {
        $tour = Tour::find($id);

        return $tour;
    }

    public function createTour($data)
    {
        $newTour = Tour::create($data);

        return $newTour;
    }

    public function updateTour($data)
    {
        $this->tour->update($data);

        $tour = $this->tour->fresh();

        return $tour;
    }

    public function deleteTour()
    {
        $this->tour->delete();
    }
}
