<?php


namespace App\Services;


use App\Reservation;
use App\Tour;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Schema;

class ReservationService
{
    private $reservation;

    public function __construct(Reservation $reservation = null)
    {
        $this->reservation = $reservation;
    }

    public function getReservations($filters)
    {
        $reservations = Reservation::all();

        $reservations = $reservations->filter(function ($reservation) use ($filters) {
            foreach ($filters as $field => $value) {
                if (Schema::hasColumn($reservation->getTable(), $field) && ($reservation->$field) != $value) {
                    return false;
                }
            }
            return true;
        });

        return $reservations;
    }

    public function createReservation($data)
    {
        $dates = explode('_', $data['dates']);
        $data['from'] = $dates[0];
        $data['till'] = $dates[1];
        $tour = Tour::find($data['tour_id']);

        $client = new Client();
        $params = [
            'form_params' => [
                'user_id' => $data['user_id'],
                'city' => $tour->city->name,
                'dates' => $dates,
                'description' => 'Tour ' . $tour->city->name,
                'charge' => $tour->cost,
            ]
        ];

        $hotelResponse = $client->post(config('app.hotel_operator_uri') . '/reservations?XDEBUG_SESSION_START=PHPSTORM', $params);

        $restaurantResponse = $client->post(config('app.restaurant_operator_uri') . '/reservations', $params);

        if ($hotelResponse->getStatusCode() == 201
            && $restaurantResponse->getStatusCode() == 201) {
            $newReservation = Reservation::create($data);
            $params['form_params']['spec_id'] = 't'.$newReservation->id;
            $paymentResponse = $client->post(config('app.payment_service') . '/charges', $params);
            return $newReservation;
        } else {
            return false;
        }
    }

    public function deleteReservation()
    {
        $this->reservation->delete();
    }
}
