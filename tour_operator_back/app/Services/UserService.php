<?php


namespace App\Services;

use App\Enums\RoleEnum;
use App\User;
use GuzzleHttp\Client;

class UserService
{
    public function createUser($data)
    {
        $user = null;
        $client = new Client();

        $params = [
            'form_params' => $data,
        ];

        $response = $client->post(config('app.user_service') . '/users', $params);

        if($response->getStatusCode() == 201) {
            $user = json_decode($response->getBody()->getContents(), true);
        }

        return $user['data'];
    }

    public function getAuthUser($token)
    {
        $user = null;

        $token = $token ?? 'fail';

        $client = new Client();

        $response = $client->get(config('app.user_service') . '/user/' . $token);

        if($response->getStatusCode() == 200) {
            $user = json_decode($response->getBody()->getContents(), true);
        } elseif($response->getStatusCode() == 302) {
            return 'auth_er';
        }

        return $user['data'];
    }

    public function login($data)
    {
        $client = new Client();

        $params = [
            'form_params' => $data,
        ];

        $response = $client->post(config('app.user_service') . '/login', $params);

        if($response->getStatusCode() == 201) {
            $token = json_decode($response->getBody()->getContents(), true)['data'];
        } elseif($response->getStatusCode() == 302) {
            return 'auth_er';
        }

        $token['user']['role'] = RoleEnum::getKey($token['user']['role']);

        return $token;
    }
}
