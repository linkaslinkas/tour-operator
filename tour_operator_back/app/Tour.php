<?php


namespace App;


use App\Transformers\TourTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Tour extends Model implements Transformable
{
    public $table = 'tours';

    protected $fillable = [
        'name',
        'description',
        'cost',
        'city_id'
    ];

    public function transformer()
    {
        return TourTransformer::class;
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
