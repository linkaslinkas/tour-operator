<?php


namespace App;

use App\Transformers\CityTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class City extends Model implements Transformable
{
    public $table = 'cities';

    protected $fillable = [
        'country_id',
        'name'
    ];

    public function transformer()
    {
        return CityTransformer::class;
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function tours()
    {
        return $this->hasMany(Tour::class);
    }
}
