<?php


namespace App\Enums;

final class RoleEnum extends BaseEnum
{
    const USER = 0;
    const MODERATOR = 1;
    const ADMIN = 2;
}
