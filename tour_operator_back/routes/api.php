<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware(['throttle:1000,1'])->prefix(config('app.api_prefix'))->group(function () {
    //User
    Route::post('users', 'UserController@store');
    Route::post('login', 'UserController@login');

    Route::group(['middleware' => 'userAuth'], function () {
        //Country
        Route::get('countries', 'CountryController@index');

        //City
        Route::get('cities', 'CityController@index');

        //Tour
        Route::get('tours', 'TourController@index');
        Route::get('tours/{id}', 'TourController@show');

        Route::group(['middleware' => 'super'], function () {
            Route::post('countries', 'CountryController@store');
            Route::post('cities', 'CityController@store');
            Route::post('tours', 'TourController@store');
            Route::put('tours/{id}', 'TourController@update');
            Route::delete('tours/{id}', 'TourController@destroy');
        });

        //Reservation
        Route::get('reservations', 'ReservationController@index');
        Route::post('reservations', 'ReservationController@store');
        Route::delete('reservations/{id}', 'ReservationController@destroy');
    });
});
