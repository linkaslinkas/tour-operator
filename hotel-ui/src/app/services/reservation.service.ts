import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IReservation, IHotel, ICountry, ICity } from '../models/reservation';


@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  httpOptions = {
    headers: new HttpHeaders({
      'Authorization': localStorage.getItem('token')
    })
  };

  constructor(private http: HttpClient) { }

  getReservations(): Observable<IReservation> {
    return this.http.get<IReservation>('http://localhost:8001/soa/service2/reservations', this.httpOptions);
  }

  deleteReservation(id: number): Observable<void> {
    return this.http.delete<void>('http://localhost:8001/soa/service2/reservations/' + id, this.httpOptions);
  }

  addReservation(data: any): Observable<void> {
    return this.http.post<void>('http://localhost:8001/soa/service2/reservations', data, this.httpOptions);
  }

  updateReservation(id: number, roomId: number): Observable<void> {
    return this.http.put<void>('http://localhost:8001/soa/service2/reservations/' + id, roomId, this.httpOptions);
  }

  getHotels(cityId: number): Observable<any> {
    return this.http.get<any>('http://localhost:8001/soa/service2/hotels?city_id=' + cityId, this.httpOptions);
  }

  getApartment(hotelId: number): Observable<any> {
    return this.http.get<any>('http://localhost:8001/soa/service2/apartments?city_id=' + hotelId, this.httpOptions);
  }

  getCountries(): Observable<any> {
    return this.http.get<any>('http://localhost:8001/soa/service2/countries', this.httpOptions);
  }

  addCountry(name: string): Observable<void> {
    return this.http.post<void>('http://localhost:8001/soa/service2/countries', {name}, this.httpOptions);
  }

  getCities(): Observable<any> {
    return this.http.get<any>('http://localhost:8001/soa/service2/cities', this.httpOptions);
  }

  addCity(countryId: number, name: string): Observable<void> {
    return this.http.post<void>('http://localhost:8001/soa/service2/cities', {country_id: countryId, name}, this.httpOptions);
  }

  addHotel(name: string, level: number, id: number) {
    return this.http.post<void>('http://localhost:8001/soa/service2/hotels', {name, level, city_id: id}, this.httpOptions);
  }

  addApartment(number: number, hotelId: number, rooms: number, cost: number) {
    return this.http.post<void>('http://localhost:8001/soa/service2/apartments', {number, hotel_id: hotelId, rooms, cost}, this.httpOptions);
  }
}
