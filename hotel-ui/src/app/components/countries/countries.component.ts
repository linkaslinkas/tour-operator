import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { AddCountriesDialogComponent } from './add-countries-dialog/add-countries-dialog.component';
import { ReservationService } from 'src/app/services/reservation.service';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.sass']
})
export class CountriesComponent implements OnInit {

  countries: any[];
  displayedColumns: string[] = ['name'];
  dataSource: MatTableDataSource<any>;

  constructor(private reservationService: ReservationService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getCountries();
  }

  getCountries(): void {
    this.reservationService.getCountries().subscribe(res => {
      this.countries = res.data;
      this.dataSource = new MatTableDataSource(this.countries);
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddCountriesDialogComponent, {
      width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.reservationService.addCountry(result).subscribe(() => {
          this.getCountries();
        });
      }
    });
  }

}
