import { MatDialogRef } from '@angular/material/dialog';
import { Component } from '@angular/core';

@Component({
  selector: 'app-add-countries-dialog',
  templateUrl: 'add-countries-dialog.component.html',
})
export class AddCountriesDialogComponent {

  name: string;

  constructor(
    public dialogRef: MatDialogRef<AddCountriesDialogComponent>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
