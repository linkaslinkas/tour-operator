import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ReservationService } from 'src/app/services/reservation.service';
import { IHotel, IApartment } from 'src/app/models/reservation';

@Component({
  selector: 'app-edit-reservation-dialog',
  templateUrl: './edit-reservation-dialog.component.html',
  styleUrls: ['./edit-reservation-dialog.component.sass']
})
export class EditReservationDialogComponent {

  hotels: IHotel[];
  hotelId: number;
  apartmentId: number;
  apartments: IApartment[];

  constructor(
    public dialogRef: MatDialogRef<EditReservationDialogComponent>,
    private reservationService: ReservationService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
      this.reservationService.getHotels(data.cityId).subscribe(res => {
        this.hotels = res.data;
      });
  }

  getApartments(hotelId: number): void {
    this.reservationService.getApartment(hotelId).subscribe(res => {
      this.apartments = res.data;
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    this.dialogRef.close({apartmentId: this.apartmentId});
  }

}
