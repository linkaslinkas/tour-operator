import { Component, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { IReservationData } from 'src/app/models/reservation';
import { ReservationService } from 'src/app/services/reservation.service';
import { MatDialog } from '@angular/material/dialog';
import { EditReservationDialogComponent } from './edit-reservation-dialog/edit-reservation-dialog.component';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.sass']
})
export class ReservationsComponent {


  displayedColumns: string[] = ['number', 'cost', 'rooms', 'from', 'till', 'action'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  reservations: IReservationData[];

  constructor(private reservationService: ReservationService, public dialog: MatDialog) {
    this.getReservations();
  }

  getReservations() {
    this.reservationService.getReservations().subscribe(res => {
      this.reservations = res.data;
      this.dataSource = new MatTableDataSource(this.reservations);
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onDelete(id: number): void {
    this.reservationService.deleteReservation(id).subscribe(() => {
      this.getReservations();
    });
  }

  openDialog(cityId: number, id: number): void {
    const dialogRef = this.dialog.open(EditReservationDialogComponent, {
      width: '250px',
      data: {
        cityId
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.reservationService.updateReservation(id, result.apartmentId).subscribe(() => {
        });
      }
    });
  }

}
