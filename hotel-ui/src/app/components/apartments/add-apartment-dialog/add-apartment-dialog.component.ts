import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-add-apartment-dialog',
  templateUrl: './add-apartment-dialog.component.html',
  styleUrls: ['./add-apartment-dialog.component.sass']
})
export class AddApartmentDialogComponent {

  number: number;
  room: number;
  cost: number;

  constructor(
    public dialogRef: MatDialogRef<AddApartmentDialogComponent>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    this.dialogRef.close({number: this.number, room: this.room, cost: this.cost});
  }

}
