import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ReservationService } from 'src/app/services/reservation.service';
import { MatDialog } from '@angular/material/dialog';
import { AddApartmentDialogComponent } from './add-apartment-dialog/add-apartment-dialog.component';

@Component({
  selector: 'app-apartments',
  templateUrl: './apartments.component.html',
  styleUrls: ['./apartments.component.sass']
})
export class ApartmentsComponent implements OnInit {

  apartments: any[];
  displayedColumns: string[] = ['number', 'room', 'cost'];
  dataSource: MatTableDataSource<any>;

  constructor(private reservationService: ReservationService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getAllApartments();
  }

  getAllApartments(): void {
    this.reservationService.getApartment(1).subscribe(res => {
      this.apartments = res.data;
      this.dataSource = new MatTableDataSource(this.apartments);
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddApartmentDialogComponent, {
      width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.reservationService.addApartment(result.number, 1, result.room, result.cost).subscribe(() => {
          this.getAllApartments();
        });
      }
    });
  }

}
