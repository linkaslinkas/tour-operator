import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { AddCitiesDialogComponent } from './add-cities-dialog/add-cities-dialog.component';
import { ReservationService } from 'src/app/services/reservation.service';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.sass']
})
export class CitiesComponent implements OnInit {


  cities: any[];
  displayedColumns: string[] = ['name', 'country'];
  dataSource: MatTableDataSource<any>;

  constructor(private reservationService: ReservationService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getCities();
  }

  getCities(): void {
    this.reservationService.getCities().subscribe(res => {
      this.cities = res.data;
      this.dataSource = new MatTableDataSource(this.cities);
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddCitiesDialogComponent, {
      width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.reservationService.addCity(result.countryId, result.name).subscribe(() => {
          this.getCities();
        });
      }
    });
  }
}
