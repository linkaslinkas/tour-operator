import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ReservationService } from 'src/app/services/reservation.service';

@Component({
  selector: 'app-add-cities-dialog',
  templateUrl: './add-cities-dialog.component.html',
  styleUrls: ['./add-cities-dialog.component.sass']
})
export class AddCitiesDialogComponent {

  name: string;
  countries: any[];
  countryId: number;

  constructor(
    public dialogRef: MatDialogRef<AddCitiesDialogComponent>,
    private reservationService: ReservationService
  ) {
      this.reservationService.getCountries().subscribe(res => {
        this.countries = res.data;
      });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    this.dialogRef.close({countryId: this.countryId, name: this.name});
  }

}
