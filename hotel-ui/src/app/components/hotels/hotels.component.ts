import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { AddHotelDialogComponent } from './add-hotel-dialog/add-hotel-dialog.component';
import { ReservationService } from 'src/app/services/reservation.service';

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.sass']
})
export class HotelsComponent implements OnInit {

  hotels: any[];
  displayedColumns: string[] = ['name'];
  dataSource: MatTableDataSource<any>;

  constructor(private reservationService: ReservationService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getAllHotels();
  }

  getAllHotels(): void {
    this.reservationService.getHotels(1).subscribe(res => {
      this.hotels = res.data;
      this.dataSource = new MatTableDataSource(this.hotels);
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddHotelDialogComponent, {
      width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.reservationService.addHotel(result.name, result.level, 1).subscribe(() => {
          this.getAllHotels();
        });
      }
    });
  }

}
