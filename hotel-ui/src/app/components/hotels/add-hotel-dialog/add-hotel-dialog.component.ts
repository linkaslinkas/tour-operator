import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AddCountriesDialogComponent } from '../../countries/add-countries-dialog/add-countries-dialog.component';

@Component({
  selector: 'app-add-hotel-dialog',
  templateUrl: './add-hotel-dialog.component.html',
  styleUrls: ['./add-hotel-dialog.component.sass']
})
export class AddHotelDialogComponent {

  name: string;
  level: string;

  constructor(
    public dialogRef: MatDialogRef<AddCountriesDialogComponent>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    this.dialogRef.close({name: this.name, level: this.level});
  }
}
