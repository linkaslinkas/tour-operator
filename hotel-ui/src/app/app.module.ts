import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { ReservationsComponent } from './components/reservations/reservations.component';
import { LoginComponent } from './components/login/login.component';
import { EditReservationDialogComponent } from './components/reservations/edit-reservation-dialog/edit-reservation-dialog.component';
import { CountriesComponent } from './components/countries/countries.component';
import { CitiesComponent } from './components/cities/cities.component';
import { AddCitiesDialogComponent } from './components/cities/add-cities-dialog/add-cities-dialog.component';
import { AddCountriesDialogComponent } from './components/countries/add-countries-dialog/add-countries-dialog.component';
import { HotelsComponent } from './components/hotels/hotels.component';
import { AddHotelDialogComponent } from './components/hotels/add-hotel-dialog/add-hotel-dialog.component';
import { ApartmentsComponent } from './components/apartments/apartments.component';
import { AddApartmentDialogComponent } from './components/apartments/add-apartment-dialog/add-apartment-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    ReservationsComponent,
    LoginComponent,
    EditReservationDialogComponent,
    CountriesComponent,
    CitiesComponent,
    AddCitiesDialogComponent,
    AddCountriesDialogComponent,
    HotelsComponent,
    AddHotelDialogComponent,
    ApartmentsComponent,
    AddApartmentDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatStepperModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatSidenavModule,
    MatListModule,
    MatFormFieldModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
