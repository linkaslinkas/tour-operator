export interface IReservation {
  data: IReservationData[];
}

export interface IReservationData {
  id: number;
  user_id: number;
  from: Date;
  till: Date;
  apartment: IApartment;
}

export interface IApartment {
  id: number;
  number: number;
  cost: number;
  rooms: number;
  hotel: IHotel;
}

export interface IHotel {
  id: number;
  name: string;
  level: number;
  city: ICity;
}

export interface ICity {
  id: number;
  name: string;
  country: ICountry;
}

export interface ICountry {
  id: number;
  name: string;
}
