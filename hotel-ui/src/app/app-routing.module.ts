import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReservationsComponent } from './components/reservations/reservations.component';
import { LoginComponent } from './components/login/login.component';
import { CountriesComponent } from './components/countries/countries.component';
import { CitiesComponent } from './components/cities/cities.component';
import { HotelsComponent } from './components/hotels/hotels.component';
import { ApartmentsComponent } from './components/apartments/apartments.component';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'reservations',
    component: ReservationsComponent
  },
  {
    path: 'countries',
    component: CountriesComponent
  },
  {
    path: 'cities',
    component: CitiesComponent
  },
  {
    path: 'hotels',
    component: HotelsComponent
  },
  {
    path: 'apartments',
    component: ApartmentsComponent
  },
  {
    path: '**',
    redirectTo: '/reservations',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
