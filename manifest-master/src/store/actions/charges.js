export const getCharges = () => ({
  type: 'GET_CHARGES',
})

export const putChargesData = (data) => ({
  type: 'PUT_CHARGES_DATA',
  payload: data,
})

