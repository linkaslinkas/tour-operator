import { takeEvery, call, put } from 'redux-saga/effects'
import axios from 'axios'

import { getCharges } from '../actions/charges'
import { getAccount } from '../actions/account'

function onPaid (data) {
  return axios
    .put('http://127.0.0.1:8003/soa/service4/charges/pay', {id:[data]})
    .then(
      response => {
        return response.data
      }
    )
}

function * putData (action) {
  try {
    yield call(onPaid, action.payload)
    yield put(getAccount())
    yield put(getCharges())
  } catch (error) {
    console.log(error)
  }
}

export function * watchPaid () {
  yield takeEvery('PAID_FOR_CHARGES', putData)
}
 