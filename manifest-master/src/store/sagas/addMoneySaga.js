import { takeEvery, call, put } from 'redux-saga/effects'
import axios from 'axios'

import { getCharges } from '../actions/charges'
import { getAccount } from '../actions/account'

function onAddMoney(data) {
  return axios
    .put('http://127.0.0.1:8003/soa/service4/accounts/update', {amount:data})
    .then(
      response => {
        return response.data.data
      }
    )
}

function * putData (action) {
  try {
    yield call(onAddMoney, action.payload)
    yield put(getAccount())
    yield put(getCharges())
  } catch (error) {
    console.log(error)
  }
}

export function * watchAddMoney () {
  yield takeEvery('ADD_MONEY', putData)
}
 