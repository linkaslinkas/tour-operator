import { takeEvery, call, put } from 'redux-saga/effects'
import axios from 'axios'

import { putLoginData } from '../actions/login'

function onLogin (data) {
  return axios
    .post('http://127.0.0.1:8003/soa/service4/login', data)
    .then(
      response => {
        axios.defaults.headers.common['Authorization'] = response.data.data.token;
        return response.data
      }
    )
}

function * putData (action) {
  try {
    const data = yield call(onLogin, action.payload)
    yield put(putLoginData(data))
  } catch (error) {
    console.log(error)
  }
}

export function * watchLogin () {
  yield takeEvery('LOGIN', putData)
}
