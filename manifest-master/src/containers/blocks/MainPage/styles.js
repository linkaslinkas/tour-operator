import { StyleSheet } from 'aphrodite'

export default StyleSheet.create({
  mainPage: {
    height: 'calc(100vh - 70px)',
    width: '100%',
  },
})