import { StyleSheet } from 'aphrodite'

export default StyleSheet.create({
  header: {
    width: '100%',
    height: '70px',
    backgroundColor: '#3f51b5',
  },
  title: {
    color: '#fff',
  },
  userName: {
    float: 'right',
  },
  container: {
    display: 'flex',
    flexDirection: 'row',
    height: '100%',
    alignItems: 'center',
  },
})
