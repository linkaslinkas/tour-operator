<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['throttle:1000,1'])->prefix(config('app.api_prefix3'))->group(function () {
    Route::post('reservations', 'ReservationController@store');
    //User
    Route::post('users', 'UserController@store');
    Route::post('login', 'UserController@login');

    Route::group(['middleware' => 'userAuth'], function () {
        //Country
        Route::get('countries', 'CountryController@index');

        //City
        Route::get('cities', 'CityController@index');

        //Restaurant
        Route::get('restaurants', 'RestaurantController@index');

        //Table
        Route::get('tables', 'TableController@index');

        //});
        Route::group(['middleware' => 'super'], function () {
            Route::post('countries', 'CountryController@store');

            Route::post('cities', 'CityController@store');
            Route::put('cities/{id}', 'CityController@update');

            Route::post('restaurants', 'RestaurantController@store');
            Route::put('restaurants/{id}', 'RestaurantController@update');
            Route::delete('restaurants/{id}', 'RestaurantController@destroy');

            Route::post('tables', 'TableController@store');
            Route::put('tables/{id}', 'TableController@update');
            Route::delete('tables/{id}', 'TableController@destroy');
        });

        //Reservation
        Route::get('reservations', 'ReservationController@index');
        Route::put('reservations/{id}', 'ReservationController@update');
        Route::delete('reservations/{id}', 'ReservationController@destroy');
    });
});
