<?php


namespace App\Services;


use App\Restaurant;
use Illuminate\Support\Facades\Schema;

class RestaurantService
{
    protected $restaurant;

    public function __construct(Restaurant $restaurant = null)
    {
        $this->restaurant = $restaurant;
    }

    public function getRestaurants(array $filters)
    {
        $restaurants = Restaurant::all();

        $restaurants = $restaurants->filter(function ($restaurant) use ($filters) {
            foreach ($filters as $field => $value) {
                if (Schema::hasColumn($restaurant->getTable(), $field) && ($restaurant->$field) != $value) {
                    return false;
                }
            }
            return true;
        });

        return $restaurants->sortByDesc('level');
    }

    public function createRestaurant($data)
    {
        $newRestaurant = Restaurant::create($data);

        return $newRestaurant;
    }

    public function updateRestaurant($data)
    {
        $this->restaurant->update($data);

        $restaurant = $this->restaurant->fresh();

        return $restaurant;
    }

    public function deleteRestaurant()
    {
        foreach ($this->restaurant->tables as $table)
        {
            (new TableService($table))->deleteTable();
        }

        $this->restaurant->delete();
    }
}
