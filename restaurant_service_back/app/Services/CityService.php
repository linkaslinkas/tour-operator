<?php


namespace App\Services;

use App\City;
use Illuminate\Support\Facades\Schema;

class CityService
{
    protected $city;

    public function __construct(City $city = null)
    {
        $this->city = $city;
    }

    public function getCities(array $filters)
    {
        $cities = City::all();

        $cities = $cities->filter(function ($city) use ($filters) {
            foreach ($filters as $field => $value) {
                if (Schema::hasColumn($city->getTable(), $field) && ($city->$field) != $value) {
                    return false;
                }
            }
            return true;
        });

        return $cities;
    }

    public function createCity($data)
    {
        $newCity = City::create($data);

        return $newCity;
    }

    public function updateCity($data)
    {
        $this->city->update($data);

        $city = $this->city->fresh();

        return $city;
    }
}
