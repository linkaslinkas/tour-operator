<?php


namespace App\Services;


use App\Table;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;

class TableService
{
    protected $table;

    public function __construct(Table $table = null)
    {
        $this->table = $table;
    }

    public function getTables(array $filters)
    {
        $tables = Table::all();

        $tables = $tables->filter(function ($table) use ($filters) {
            foreach ($filters as $field => $value) {
                if (Schema::hasColumn($table->getTable(), $field) && ($table->$field) != $value) {
                    return false;
                }
            }
            return true;
        });

        return $tables;
    }

    public function createTable($data)
    {
        $newTable = Table::create($data);

        return $newTable;
    }

    public function updateTable($data)
    {
        $this->table->update($data);

        $table = $this->table->fresh();

        return $table;
    }

    public function getFreeTable($data)
    {
        $tables = Table::whereHas('restaurant', function ($query) use ($data) {
            return $query->whereHas('city', function ($q) use ($data) {
                return $q->where(['cities.name' => $data['city']]);
            });
        })->orderBy('cost')->get();

        $table = $this->checkIfFree($tables, $data['dates']);

        return $table;
    }

    private function checkIfFree($tables, $dates)
    {
        foreach ($tables as $table) {
            foreach ($table->reservations as $reservation) {
                if($reservation->from > Carbon::createFromFormat('Y-m-d', $dates[0])
                    || $reservation->till <= Carbon::createFromFormat('Y-m-d', $dates[1])) {
                    return false;
                }
            }
            return $table;
        }
    }

    public function deleteTable()
    {
        foreach($this->table->reservations as $reservation) {
            (new ReservationService($reservation))->deleteReservation();
        }

        $this->table->delete();
    }
}
