<?php


namespace App;


use App\Transformers\RestaurantTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model implements Transformable
{
    public $table = 'restaurants';

    protected $fillable = [
        'city_id',
        'level',
        'name'
    ];

    public function transformer()
    {
        return RestaurantTransformer::class;
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function tables()
    {
        return $this->hasMany(Table::class);
    }
}
