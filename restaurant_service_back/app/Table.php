<?php


namespace App;


use App\Transformers\TableTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Table extends Model implements Transformable
{
    public $table = 'tables';

    protected $fillable = [
        'restaurant_id',
        'cost',
        'number'
    ];

    public function transformer()
    {
        return TableTransformer::class;
    }

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }
}
