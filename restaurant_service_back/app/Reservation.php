<?php


namespace App;


use App\Transformers\ReservationTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model implements Transformable
{
    public $tableName = 'reservations';

    protected $fillable = [
        'user_id',
        'table_id',
        'from',
        'till'
    ];

    public function transformer()
    {
        return ReservationTransformer::class;
    }

    public function table()
    {
        return $this->belongsTo(Table::class);
    }
}
