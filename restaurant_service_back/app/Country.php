<?php


namespace App;

use App\Transformers\CountryTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Country extends Model implements Transformable
{
    public $table = 'countries';

    protected $fillable = [
        'name'
    ];

    public function transformer()
    {
        return CountryTransformer::class;
    }

    public function cities(){
        return $this->hasMany(City::class);
    }
}
