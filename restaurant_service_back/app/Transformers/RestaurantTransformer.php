<?php


namespace App\Transformers;

use App\Restaurant;
use Flugg\Responder\Transformers\Transformer;

class RestaurantTransformer extends Transformer
{
    protected $relations = [];

    protected $load = [
        'city' => CityTransformer::class
    ];

    public function transform(Restaurant $restaurant)
    {
        return [
            'id' => $restaurant->id,
            'name' => $restaurant->name,
            'level' => $restaurant->level
        ];
    }

}
