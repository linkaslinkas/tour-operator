<?php


namespace App\Transformers;


use App\Table;
use Flugg\Responder\Transformers\Transformer;

class TableTransformer extends Transformer
{
    protected $relations = [];

    protected $load = [
        'restaurant' => RestaurantTransformer::class
    ];

    public function transform(Table $table)
    {
        return [
            'id' => $table->id,
            'number' => $table->number,
            'cost' => $table->cost
        ];
    }

}
