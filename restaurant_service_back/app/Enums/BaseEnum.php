<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

class BaseEnum extends Enum
{
    public static function getKey($value, $cast = 'int'): string
    {
        switch ($cast) {
            case 'int':
                return strtolower(parent::getKey((int)$value));
            case 'string':
                return strtolower(parent::getKey((string)$value));
        }
    }

    public static function getValue(string $key)
    {
        return parent::getValue(strtoupper($key));
    }
}
