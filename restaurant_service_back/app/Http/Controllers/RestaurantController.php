<?php


namespace App\Http\Controllers;

use App\Restaurant;
use App\Services\HotelService;
use App\Services\RestaurantService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RestaurantController extends Controller
{
    const INDEX_RULES = [
        'city_id' => 'integer|exists:cities,id',
        'level' => 'float'
    ];
    public function index(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::INDEX_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $restaurantService = new RestaurantService;
        $restaurants = $restaurantService->getRestaurants($params);

        return responder()->success($restaurants)->respond(200);
    }

    const STORE_RULES = [
        'city_id' => 'integer|exists:cities,id',
        'name' => 'required|string',
        'level' => 'required'
    ];
    public function store(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::STORE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $restaurantService = new RestaurantService;
        $newRestaurant = $restaurantService->createRestaurant($params);

        return responder()->success($newRestaurant)->respond(201);
    }

    const UPDATE_RULES = [
        'id' => 'required|exists:restaurants,id',
        'name' => 'string',
        'level' => 'float',
        'city_id' => 'exists:cities,id'
    ];
    public function update(Request $request, $id)
    {
        $params = $request->all();
        $params['id'] = $id;

        $validator = Validator::make($params, self::UPDATE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }
        $restaurant = Restaurant::find($id);

        $restaurantService = new RestaurantService($restaurant);
        $newRestaurant = $restaurantService->updateRestaurant($params);

        return responder()->success($newRestaurant)->respond(200);
    }

    const DESTROY_RULES = [
        'id' => 'required|exists:restaurants,id'
    ];
    public function destroy(Request $request, $id)
    {
        $params = $request->all();
        $params['id'] = $id;

        $validator = Validator::make($params, self::DESTROY_RULES);
        if ($validator->fails()) {
            return responder()->error('validation error')->respond(403);
        }
        $restaurant = Restaurant::find($id);

        $restaurantService = new RestaurantService($restaurant);
        $restaurantService->deleteRestaurant();

        return responder()->success(null)->respond(204);
    }
}
