<?php


namespace App\Http\Controllers;


use App\Services\TableService;
use App\Table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TableController extends Controller
{
    const INDEX_RULES = [
        'restaurant_id' => 'integer|exists:restaurants,id',
    ];
    public function index(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::INDEX_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $tableService = new TableService;
        $tables = $tableService->getTables($params);

        return responder()->success($tables)->respond(200);
    }

    const STORE_RULES = [
        'restaurant_id' => 'integer|exists:restaurants,id',
        'number' => 'required|integer',
    ];
    public function store(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::STORE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $tableService = new TableService();
        $newTable = $tableService->createTable($params);

        return responder()->success($newTable)->respond(201);
    }

    const UPDATE_RULES = [
        'id' => 'required|exists:tables,id',
        'number' => 'integer',
        'restaurant_id' => 'integer|exists:restaurants,id',
    ];
    public function update(Request $request, $id)
    {
        $params = $request->all();
        $params['id'] = $id;

        $validator = Validator::make($params, self::UPDATE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }
        $restaurant = Table::find($id);

        $tableService = new TableService($restaurant);
        $newTable = $tableService->updateTable($params);

        return responder()->success($newTable)->respond(200);
    }

    const DELETE_RULES = [
        'id' => 'required|exists:tables,id'
    ];
    public function destroy(Request $request, $id)
    {
        $params = $request->all();
        $params['id'] = $id;

        $validator = Validator::make($params, self::DELETE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation error')->respond(403);
        }
        $table = Table::find($id);

        $tableService = new TableService($table);
        $tableService->deleteTable();

        return responder()->success(null)->respond(204);
    }
}
