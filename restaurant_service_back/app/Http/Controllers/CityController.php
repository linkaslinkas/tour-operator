<?php


namespace App\Http\Controllers;


use App\City;
use App\Services\CityService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CityController extends Controller
{
    const INDEX_RULES = [
        'country_id' => 'exists:countries,id',
        'name' => 'string'
    ];
    public function index(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::INDEX_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $cityService = new CityService;
        $cities = $cityService->getCities($params);

        return responder()->success($cities)->respond(200);
    }

    const STORE_RULES = [
        'country_id' => 'required|exists:countries,id',
        'name' => 'required|string'
    ];
    public function store(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::STORE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $cityService = new CityService;
        $newCity = $cityService->createCity($params);

        return responder()->success($newCity)->respond(201);
    }

    const UPDATE_RULES = [
        'id' => 'required|exists:cities,id',
        'name' => 'string'
    ];
    public function update(Request $request, $id)
    {
        $params = $request->all();
        $params['id'] = $id;

        $validator = Validator::make($params, self::UPDATE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }
        $hotel = City::find($id);

        $cityService = new CityService($hotel);
        $newCity = $cityService->updateCity($params);

        return responder()->success($newCity)->respond(200);
    }
}
