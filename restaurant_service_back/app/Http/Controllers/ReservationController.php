<?php


namespace App\Http\Controllers;

use App\Enums\RoleEnum;
use App\Reservation;
use App\Services\ReservationService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReservationController extends Controller
{

    const INDEX_RULES = [

    ];
    public function index(Request $request)
    {
        $params = $request->all();
        $token = $request->header('Authorization');
        $authUser = (new UserService())->getAuthUser($token);

        if(is_array($authUser) && $authUser['role'] == RoleEnum::USER) {
            $params['user_id'] = $authUser['id'];
        }

        $validator = Validator::make($params, self::INDEX_RULES);
        if ($validator->fails()) {
            return responder()->error('validation error', $validator->errors()->first())->respond(400);
        }

        $reservationService = new ReservationService;
        $reservations = $reservationService->getReservations($params);

        return responder()->success($reservations)->respond(200);
    }

    const STORE_RULES = [
        'user_id' => 'required',
        'dates' => 'required|array',
        'dates.*' => 'string'
    ];
    public function store(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::STORE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $reservationService = new ReservationService();
        $newReservation = $reservationService->createReservation($params);

        if($newReservation) {
            return responder()->success($newReservation)->respond(201);
        } else {
            return responder()->success('not_created')->respond(400);
        }
    }

    const UPDATE_RULES = [
        'id' => 'required|exists:reservations,id',
        'table_id' => 'exists:tables,id'
    ];
    public function update(Request $request, $id)
    {
        $params = $request->all();
        $params['id'] = $id;

        $token = $request->header('Authorization');
        $authUser = (new UserService())->getAuthUser($token);

        $validator = Validator::make($params, self::UPDATE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $reservation = Reservation::find($id);
        if(!is_array($authUser) || $authUser['id'] != $reservation->user_id) {
            return responder()->error('permission_error')->respond(403);
        }

        $reservationService = new ReservationService($reservation);
        $newReservation = $reservationService->updateReservation($params);

        return responder()->success($newReservation)->respond(201);
    }

    const DESTROY_RULES = [
        'id' => 'required|exists:reservations,id'
    ];
    public function destroy(Request $request, $id)
    {
        $params = $request->all();
        $params['id'] = $id;

        $token = $request->header('Authorization');
        $authUser = (new UserService())->getAuthUser($token);

        $validator = Validator::make($params, self::DESTROY_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $reservation = Reservation::find($id);

        if(!is_array($authUser) || $authUser['id'] != $reservation->user_id) {
            return responder()->error('permission_error')->respond(403);
        }

        $reservationService = new ReservationService($reservation);
        $reservationService->deleteReservation();

        return responder()->success(null)->respond(204);
    }
}
