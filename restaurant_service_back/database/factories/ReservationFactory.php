<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Table;
use App\Reservation;
use App\User;
use Faker\Generator as Faker;

$factory->define(Reservation::class, function (Faker $faker) {
    return [
        'table_id' => factory(Table::class)->create()->id,
        'user_id' => 1
    ];
});
