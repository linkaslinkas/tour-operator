<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Table;
use App\Restaurant;
use Faker\Generator as Faker;

$factory->define(Table::class, function (Faker $faker) {
    return [
        'number' => $faker->randomNumber(2),
        'rooms' => $faker->numberBetween(1,5),
        'cost' => $faker->numberBetween(0),
        'restaurant_id' => factory(Restaurant::class)->create()->id,
    ];
});
