<?php


namespace App\Http\Controllers;

use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    const INDEX_RULES = [

    ];
    public function index(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::INDEX_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $userService = new UserService;
        $users = $userService->getUsers($params);

        return responder()->success($users)->respond(200);
    }

    const STORE_RULES = [
        'name' => 'required|string',
        'email' => 'required|string',
        'password' => 'required'
    ];
    public function store(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::STORE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $userService = new UserService;
        $newUser = $userService->createUser($params);

        return responder()->success($newUser)->respond(201);
    }

    const SHOW_RULES = [
        'id' => 'required|exists:users,id'
    ];
    public function show(Request $request, $id)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::SHOW_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $user = User::find($id);

        return responder()->success($user)->respond(200);
    }

    const AUTH_RULES = [
        'token' => 'string'
    ];
    public function getAuthUser(Request $request, $token)
    {
        $params = $request->all();
        $params['token'] = $token;

        $validator = Validator::make($params, self::AUTH_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $userService = new UserService;
        $user = $userService->getAuthUser($token);

        if($user) {
            return responder()->success($user)->respond(200);
        } else {
            return responder()->success()->respond(302);
        }
    }

    public function login(Request $request)
    {
        $params = $request->all();

        $userService = new UserService;
        $token = $userService->login($params);

        if($token) {
            return responder()->success($token)->respond(201);
        } else {
            return responder()->success()->respond(302);
        }
    }
}
