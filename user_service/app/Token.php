<?php


namespace App;


use App\Transformers\TokenTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Token extends Model implements Transformable
{
    public $table = 'auth_token';

    protected $fillable = [
        'token',
        'user_id',
        'expires_at'
    ];

    public function transformer()
    {
        return TokenTransformer::class;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
