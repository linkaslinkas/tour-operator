<?php

namespace App\Services;

use App\Token;
use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;

class UserService
{
    public function getUsers($filters)
    {
        $users = User::all();

        return $users;
    }

    public function createUser($data)
    {
        $data['password'] = crypt($data['password'], 'salt');
        $user = User::create($data);

        $client = new Client();

        $params = [
            'form_params' => [
                'user_id' => $user->id
            ]
        ];

        $client->post(config('app.payment_service_uri') . '/accounts', $params);

        return $user;
    }

    public function getAuthUser($token)
    {
        $token = Token::where(['token' => $token])->first();

        if(!$token || $token->expires_at < Carbon::now()->toDateTimeString()) {
            return null;
        }

        $user = User::find($token->user_id);

        return $user;
    }

    public function login($data)
    {
        $password = crypt($data['password'], 'salt');

        $user = User::where(['email' => $data['email'], 'password' => $password])->first();

        if($user) {
            $expires_at = Carbon::now()->addSeconds(86400)->toDateTime();
            return Token::create([
                'token' => md5(time()),
                'user_id' => $user->id,
                'expires_at' => $expires_at
            ]);
        } else {
            return null;
        }
    }
}
