<?php

namespace App\Transformers;

use App\City;
use App\User;
use Flugg\Responder\Transformers\Transformer;

class UserTransformer extends Transformer
{
    protected $relations = [];

    protected $load = [];

    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'role' => $user->role
        ];
    }
}
