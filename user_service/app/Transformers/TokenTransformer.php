<?php


namespace App\Transformers;


use App\Token;
use Flugg\Responder\Transformers\Transformer;

class TokenTransformer extends Transformer
{
    protected $relations = [];

    protected $load = [
        'user' => UserTransformer::class
    ];

    public function transform(Token $token)
    {
        return [
            'id' => $token->id,
            'token' => $token->token
        ];
    }
}
