<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['throttle:1000,1'])->prefix(config('app.api_prefix4'))->group(function () {
    Route::post('accounts', 'AccountController@store');
    Route::post('charges', 'ChargeController@store');
    Route::put('charges/update', 'ChargeController@update');

    //User
    Route::post('users', 'UserController@store');
    Route::post('login', 'UserController@login');

    Route::group(['middleware' => 'userAuth'], function () {
        Route::get('accounts', 'AccountController@index');
        Route::put('accounts/update', 'AccountController@update');

        Route::get('cards', 'UserCardController@index');
        Route::post('cards', 'UserCardController@store');
        Route::delete('cards/{id}', 'UserCardController@destroy');

        Route::get('charges', 'ChargeController@index');
        Route::put('charges/pay', 'ChargeController@payCharge');
    });
});
