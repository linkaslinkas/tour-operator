<?php


namespace App\Transformers;


use App\Account;
use App\UserCard;
use Flugg\Responder\Transformers\Transformer;

class UserCardTransformer extends Transformer
{
    protected $relations = [];

    protected $load = [

    ];

    public function transform(UserCard $userCard)
    {
        return [
            'id' => $userCard->id,
            'account_id' => $userCard->account_id,
            'number' => $userCard->number,
            'date' => $userCard->date,
            'owner' => $userCard->owner
        ];
    }

}
