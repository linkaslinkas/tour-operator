<?php


namespace App\Transformers;


use App\Charge;
use Flugg\Responder\Transformers\Transformer;

class ChargeTransformer extends Transformer
{
    protected $relations = [];

    protected $load = [];

    public function transform(Charge $charge)
    {
        return [
            'id' => $charge->id,
            'user_id' => $charge->user_id,
            'description' => $charge->description,
            'charge' => $charge->charge
        ];
    }
}
