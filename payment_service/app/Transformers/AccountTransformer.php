<?php


namespace App\Transformers;


use App\Account;
use Flugg\Responder\Transformers\Transformer;

class AccountTransformer extends Transformer
{
    protected $relations = [];

    protected $load = [];

    public function transform(Account $account)
    {
        return [
            'id' => $account->id,
            'user_id' => $account->user_id,
            'amount' => $account->amount,
        ];
    }
}
