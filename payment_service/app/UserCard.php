<?php


namespace App;

use App\Transformers\UserCardTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class UserCard extends Model implements Transformable
{
    public $table = 'users_cards';

    protected $fillable = [
        'account_id',
        'number',
        'date',
        'owner'
    ];

    public function transformer()
    {
        return UserCardTransformer::class;
    }

    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
