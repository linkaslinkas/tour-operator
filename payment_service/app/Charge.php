<?php


namespace App;


use App\Transformers\ChargeTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Charge extends Model implements Transformable
{
    public $table = 'charges';

    protected $fillable = [
        'user_id',
        'description',
        'charge',
        'is_paid',
        'spec_id'
    ];

    public function transformer()
    {
        return ChargeTransformer::class;
    }
}
