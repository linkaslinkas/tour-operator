<?php

namespace App;

use App\Transformers\AccountTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;

class Account extends Model implements Transformable
{
    public $table = 'accounts';

    protected $fillable = [
        'user_id',
        'amount'
    ];

    public function transformer()
    {
        return AccountTransformer::class;
    }

    public function cards()
    {
        $this->hasMany(UserCard::class);
    }
}
