<?php


namespace App\Http\Controllers;


use App\Charge;
use App\Enums\RoleEnum;
use App\Services\ChargeService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ChargeController extends Controller
{
    const INDEX_RULES = [
        'user_id' => 'required|integer'
    ];

    public function index(Request $request)
    {
        $params = $request->all();

        $token = $request->header('Authorization');
        $authUser = (new UserService())->getAuthUser($token);

        if (is_array($authUser) && $authUser['role'] == RoleEnum::USER) {
            $params['user_id'] = $authUser['id'];
        } else {
            unset($params['user_id']);
        }

        $validator = Validator::make($params, self::INDEX_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $accountService = new ChargeService();
        $account = $accountService->getCharges($params);

        return responder()->success($account)->respond(200);
    }

    const STORE_RULES = [
        'user_id' => 'required|integer',
        'description' => 'required|string',
        'charge' => 'required|integer'
    ];

    public function store(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::STORE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $chargeService = new ChargeService;
        $newCharge = $chargeService->createCharge($params);

        if ($newCharge) {
            return responder()->success($newCharge)->respond(201);
        } else {
            return responder()->error()->respond(400);
        }
    }

    const PAID_RULES = [
        'id' => 'required|array',
        'id.*' => 'exists:charges,id'
    ];
    public function payCharge(Request $request)
    {
        $params = $request->all();

        $token = $request->header('Authorization');
        $authUser = (new UserService())->getAuthUser($token);

        $validator = Validator::make($params, self::PAID_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $chargeService = new ChargeService();
        $res = $chargeService->pay($authUser, $params['id']);

        if($res === 'money') {
            return responder()->error('not_enough_money')->respond(400);
        } else {
            return responder()->success()->respond(200);
        }
    }

    const UPDATE_RULES = [
        'spec_id' => 'required'
    ];
    public function update(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::UPDATE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $charge = Charge::where('spec_id', $params['spec_id'])->first();

        $chargeService = new ChargeService($charge);
        $chargeService->update($params);

        return responder()->success()->respond(200);
    }
}
