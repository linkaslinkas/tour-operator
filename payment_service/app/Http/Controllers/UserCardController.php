<?php


namespace App\Http\Controllers;


use App\Account;
use App\Enums\RoleEnum;
use App\Services\UserCardService;
use App\Services\UserService;
use App\UserCard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserCardController extends Controller
{
    const INDEX_RULES = [
        'user_id' => 'required|integer',
        'account_id' => 'required|exists:accounts,id'
    ];
    public function index(Request $request)
    {
        $params = $request->all();

        $token = $request->header('Authorization');
        $authUser = (new UserService())->getAuthUser($token);

        if(is_array($authUser) && $authUser['role'] == RoleEnum::USER) {
            $params['user_id'] = $authUser['id'];
        } else {
            unset($params['user_id']);
        }

        $account = Account::where('user_id', $params['user_id'])->first();
        $params['account_id'] = $account->id;

        $validator = Validator::make($params, self::INDEX_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $userCardService = new UserCardService;
        $cards = $userCardService->getCards($params);

        return responder()->success($cards)->respond(200);
    }

    const STORE_RULES = [
        'user_id' => 'required|integer',
        'number' => 'required|string',
        'date' => 'required|string',
        'owner' => 'required|string'
    ];
    public function store(Request $request)
    {
        $params = $request->all();

        $token = $request->header('Authorization');
        $authUser = (new UserService())->getAuthUser($token);

        if(is_array($authUser) && $authUser['role'] == RoleEnum::USER) {
            $params['user_id'] = $authUser['id'];
        } else {
            unset($params['user_id']);
        }

        $validator = Validator::make($params, self::STORE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $account = Account::where('user_id', $params['user_id'])->first();
        $params['account_id'] = $account->id;

        $userCardService = new UserCardService();
        $newCard = $userCardService->createCard($params);

        return responder()->success($newCard)->respond(201);
    }

    const DELETE_RULES = [
        'id' => 'required|exists:users_cards,id'
    ];
    public function destroy(Request $request, $id)
    {
        $params = $request->all();
        $params['id'] = $id;

        $token = $request->header('Authorization');
        $authUser = (new UserService())->getAuthUser($token);

        $validator = Validator::make($params, self::DELETE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $card = UserCard::find($id);

        if(!is_array($authUser) || $authUser['id'] != $card->account->user_id) {
            return responder()->error('permission_error')->respond(403);
        }

        $userCardService = new UserCardService($card);
        $userCardService->deleteCard();

        return responder()->success(null)->respond(204);
    }
}
