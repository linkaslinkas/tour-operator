<?php


namespace App\Http\Controllers;


use App\Account;
use App\Enums\RoleEnum;
use App\Services\AccountService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    const INDEX_RULES = [
        'user_id' => 'required|integer'
    ];
    public function index(Request $request)
    {
        $params = $request->all();

        $token = $request->header('Authorization');
        $authUser = (new UserService())->getAuthUser($token);

        if(is_array($authUser) && $authUser['role'] == RoleEnum::USER) {
            $params['user_id'] = $authUser['id'];
        } else {
            unset($params['user_id']);
        }

        $validator = Validator::make($params, self::INDEX_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $accountService = new AccountService;
        $account = $accountService->getAccount($params);

        return responder()->success($account)->respond(200);
    }

    const STORE_RULES = [
        'user_id' => 'required|integer'
    ];
    public function store(Request $request)
    {
        $params = $request->all();

        $validator = Validator::make($params, self::STORE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $accountService = new AccountService;
        $newAccount = $accountService->createAccount($params);

        if($newAccount) {
            return responder()->success($newAccount)->respond(201);
        } else {
            return responder()->error()->respond(400);
        }
    }

    const UPDATE_RULES = [
        'user_id' => 'required|integer',
        'amount' => 'integer'
    ];
    public function update(Request $request)
    {
        $params = $request->all();

        $token = $request->header('Authorization');
        $authUser = (new UserService())->getAuthUser($token);

        if(is_array($authUser) && $authUser['role'] == RoleEnum::USER) {
            $params['user_id'] = $authUser['id'];
        } else {
            unset($params['user_id']);
        }

        $validator = Validator::make($params, self::UPDATE_RULES);
        if ($validator->fails()) {
            return responder()->error('validation_error', $validator->errors()->first())->respond(400);
        }

        $account = Account::where('user_id', $params['user_id'])->first();

        $accountService = new AccountService($account);
        $newAccount = $accountService->updateAccount($params);

        return responder()->success($newAccount)->respond(200);
    }
}
