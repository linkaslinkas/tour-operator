<?php


namespace App\Enums;


class ChargeStatus extends BaseEnum
{
    const UNPAID = 0;
    const PAID = 1;
}
