<?php


namespace App\Services;


use App\Account;
use App\Charge;
use App\Enums\ChargeStatus;
use Illuminate\Support\Facades\Schema;

class ChargeService
{
    private $charge;

    public function __construct(Charge $charge = null)
    {
        $this->charge = $charge;
    }

    public function getCharges($filters)
    {
        $charges = Charge::all();
        $filters['is_paid'] = ChargeStatus::UNPAID;

        $charges = $charges->filter(function ($charge) use ($filters) {
            foreach ($filters as $field => $value) {
                if (Schema::hasColumn($charge->getTable(), $field) && ($charge->$field) != $value) {
                    return false;
                }
            }
            return true;
        });
        return $charges;
    }

    public function createCharge($data)
    {
        $newCharge = Charge::create($data);

        return $newCharge;
    }

    public function pay($authUser, $ids)
    {
        $account = Account::where('user_id', $authUser['id'])->first();
        $cost = 0;
        $balance = $account->amount;

        $charges = Charge::whereIn('id', $ids)->get();

        foreach($charges as $charge) {
            $cost += $charge->charge;

            if ($cost > $balance) {
                return 'money';
            }
        }

        foreach($charges as $charge) {
            $charge->update(['is_paid' => ChargeStatus::PAID]);
        }

        (new AccountService($account))->updateAccount(['amount' => $cost*(-1)]);
        return true;
    }

    public function update($data)
    {
        $this->charge->update($data);

        return $this->charge->fresh();
    }
}
