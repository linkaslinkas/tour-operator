<?php


namespace App\Services;


use App\UserCard;
use Illuminate\Support\Facades\Schema;

class UserCardService
{
    private $card;

    public function __construct(UserCard $card = null)
    {
        $this->card = $card;
    }

    public function getCards($filters)
    {
        $cards = UserCard::all();

        $cards = $cards->filter(function ($card) use ($filters) {
            foreach ($filters as $field => $value) {
                if (Schema::hasColumn($card->getTable(), $field) && ($card->$field) != $value) {
                    return false;
                }
            }
            return true;
        });

        return $cards->first();
    }

    public function createCard($data)
    {
        $newCard = UserCard::create($data);

        return $newCard;
    }

    public function deleteCard()
    {
        $this->card->delete();
    }
}
