<?php


namespace App\Services;


use App\Account;
use Illuminate\Support\Facades\Schema;

class AccountService
{
    private $account;

    public function __construct(Account $account = null)
    {
        $this->account = $account;
    }

    public function getAccount($filters)
    {
        $accounts = Account::all();

        $accounts = $accounts->filter(function ($account) use ($filters) {
            foreach ($filters as $field => $value) {
                if (Schema::hasColumn($account->getTable(), $field) && ($account->$field) != $value) {
                    return false;
                }
            }
            return true;
        });

        return $accounts->first();
    }

    public function createAccount($data)
    {
        $newAccount = Account::create($data);

        return $newAccount;
    }

    public function updateAccount($data)
    {
        if(isset($data['amount'])) {
            $data['amount'] += $this->account->amount;
        }

        if($data['amount'] < 0) {
            return null;
        }

        $this->account->update($data);

        $account = $this->account->fresh();

        return $account;
    }
}
